<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property string $text
 * @property User $user_id
 */
class Tasks extends Model
{
    protected $fillable = [
        'text', 'user_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
