<?php

namespace App\Http\Controllers;

use App\Entity\Tasks;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        //$query = Tasks::with("user")->orderByDesc('id');
        $query = Tasks::join('users', 'tasks.user_id', '=', 'users.id')/*->orderByDesc('tasks.id')*/;
        if (($value = $request->get('status'))!=NULL) {
            $query->where('status', $value);
        }
        /*$query->whereHas("user", function($query) use ($request)
        {
            if (!empty($value = $request->get('name'))) {
                //$query->where('users.name', 'like', '%' . $value . '%');
            }
            if (!empty($value = $request->get('email'))) {
                $query->where('users.email', 'like', '%' . $value . '%');
            }

        });*/

        if (!empty($value = $request->get('name'))) {
            $query->orderBy('users.name', $value);
        }
        if (!empty($value = $request->get('email'))) {
            $query->orderBy('users.email', $value);
        }
        $tasks = $query->paginate(3);
        return view('home', compact('tasks'));
    }
}
