<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use App\Entity\User;
use App\Entity\Tasks;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function create()
    {
        $users = User::orderByDesc('id')->get();
        return view('task.create',compact('users'));
    }
    public function store(Request $request)
    {

        $this->validate($request, [
            'text' => 'required|string',
            'user' => 'required|exists:users,id',
        ]);
        Tasks::create([
            'text' => $request['text'],
            'user_id' => $request['user'],
            'status' => 0
        ]);

        return redirect()->route('home');
    }

    public function edit(Tasks $task)
    {
        return view('task.edit',compact('task'));
    }
    public function update(Request $request, Tasks $task)
    {
        $request['status']=$request['status']?1:0;
        $this->validate($request, [
            'text' => 'required|string',
            'status' => 'required|boolean',
        ]);

        $task->update([
            'text' => $request['text'],
            'status' => $request['status'],
        ]);

        return redirect()->route('home');
    }
}
