@extends('layouts.app')
@section('search')
    <div class="search-bar pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <form action="{{ route('home') }}" method="GET">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    Name
                                    <!--<input type="text" class="form-control" name="name"
                                           value="{{ request('name') }}" placeholder="Filter for name">-->
                                        <select id="name" class="form-control" name="name">
                                            <option value=""></option>
                                            <option value="asc"{{ "asc" == request('name') ? ' selected' : '' }}>asc</option>
                                            <option value="desc"{{ "desc" == request('name') ? ' selected' : '' }}>desc</option>
                                        </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Email
                                    <!--<input type="text" class="form-control" name="email"
                                           value="{{ request('email') }}" placeholder="Filter for email">-->
                                    <select id="email" class="form-control" name="email">
                                        <option value=""></option>
                                        <option value="asc"{{ "asc" == request('email') ? ' selected' : '' }}>asc</option>
                                        <option value="desc"{{ "desc" == request('email') ? ' selected' : '' }}>desc</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    Status
                                    <select id="status" class="form-control" name="status">
                                        <option value=""></option>

                                            <option value="0"{{ "0" === request('status') ? ' selected' : '' }}>{{ 0 }}</option>
                                            <option value="1"{{ "1" === request('status') ? ' selected' : '' }}>{{ 1 }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <button class="btn btn-light border" type="submit">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3" style="text-align: right">
                    <p><a href="{{ route('task.create') }}" class="btn btn-success"><span class="fa fa-plus"></span>
                            Add task</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Text</th>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($tasks as $task)
            <tr>
                <td>{{ $task->text }}</td>
                <td>{{ $task->user->name }}</td>
                <td>{{ $task->user->email }}</td>
                <td>{{ $task->status }}</td>
                <td><a href="{{ route('task.edit', $task) }}">Edit</a></td>
            </tr>
        @endforeach

        </tbody>
    </table>
    {{ $tasks->links() }}
@endsection
