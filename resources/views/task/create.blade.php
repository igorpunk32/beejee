@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('task.create.store') }}">
        @csrf
        <div class="form-group">
            <label for="text" class="col-form-label">Text</label>
            <input id="name" class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" name="text" value="{{ old('text') }}" required>
            @if ($errors->has('text'))
                <span class="invalid-feedback"><strong>{{ $errors->first('text') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="parent" class="col-form-label">User</label>
            <select id="parent" class="form-control{{ $errors->has('user') ? ' is-invalid' : '' }}" name="user" required>
                @foreach ($users as $user)
                    <option value="{{ $user->id }}"{{ $user->id == old('user') ? ' selected' : '' }}>
                        {{ $user->name }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('user'))
                <span class="invalid-feedback"><strong>{{ $errors->first('user') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
