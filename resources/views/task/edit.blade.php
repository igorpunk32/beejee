@extends('layouts.app')
@section('content')
    <form method="POST" action="{{ route('task.edit.update',$task) }}">
        @csrf
        <div class="form-group">
            <label for="text" class="col-form-label">Text</label>
            <input id="text" class="form-control{{ $errors->has('text') ? ' is-invalid' : '' }}" name="text" value="{{ old('text',$task->text) }}" required>
            @if ($errors->has('text'))
                <span class="invalid-feedback"><strong>{{ $errors->first('text') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <div class="checkbox">
                <label>
                    <input id="status" name="status" type="checkbox" {{ old('status', $task->status) ? 'checked' : '' }}> Status
                </label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
