<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Auth::routes();
Route::group(
    [
        'prefix' => 'task',
        'as' => 'task.',
        'namespace' => 'Task',
    ],
    function () {
        Route::get('/create', 'TaskController@create')->name('create');
        Route::post('/create', 'TaskController@store')->name('create.store');
        Route::get('/edit/{task}', 'TaskController@edit')->middleware('auth', 'can:admin')->name('edit');
        Route::post('/edit/{task}', 'TaskController@update')->middleware('auth', 'can:admin')->name('edit.update');
    }
);
